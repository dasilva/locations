//
//  AppCoordinator.swift
//  Locations
//
//  Created by João Pedro Silva on 14/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.

import Foundation
import UIKit

protocol AppCoordinatorProtocol: Coordinator {
    
    init (window: UIWindow, launchOptions: [UIApplication.LaunchOptionsKey: Any]?)
}

class AppCoordinator: AppCoordinatorProtocol {
 
    //MARK: - Variables
    let window: UIWindow
    var childCoordinator: Coordinator?
    var repository: Repository = Repository(with: NetworkManager(), and: PersistanceManager())
    
    // In order to be a subclass of the Coordinator protocol we should implement the Load Method, however in this case
    // We don t implemented because this class handle with the window of the app directly
    func load() -> UIViewController {
        return UIViewController()
    }
    
    //MARK: - Initializers
    required init(window: UIWindow, launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        
        //Handle Notifications
        self.window = window
        
        //Configs
        AppStyleConfigurator.config()
        
        let locationsCoordinator = LocationsListCoordinator(with: repository)
        route(to: locationsCoordinator)
    }
    
    private func route(to coordinator: Coordinator){
        
        childCoordinator = coordinator
        window.rootViewController = coordinator.load()
        window.makeKeyAndVisible()
    }
}

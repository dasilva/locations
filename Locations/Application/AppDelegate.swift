//
//  AppDelegate.swift
//  Locations
//
//  Created by João Pedro Silva on 14/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var appCoordinator: AppCoordinatorProtocol?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        let window = UIWindow(frame: UIScreen.main.bounds)
        appCoordinator = AppCoordinator(window: window, launchOptions: launchOptions)
        self.window = window
        
        return true
    }
}

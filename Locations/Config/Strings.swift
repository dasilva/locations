//
//  Strings.swift
//  Locations
//
//  Created by João Pedro Silva on 19/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.
//

import Foundation

public enum Strings{
    
    enum Suggestions{
        
        static let title = "Suggestions"
    }
    
    enum Location{
        
        static let title = "Location Detail"
    }
}

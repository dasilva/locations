//
//  Location.swift
//  Locations
//
//  Created by João Pedro Silva on 19/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.
//

import Foundation

//MARK: Response
struct CompleteResponse: Decodable {

    let view: [ViewCodable]

    enum CodingKeys: String, CodingKey {
        case view = "View"
        case response = "Response"
    }

    init(from decoder: Decoder) throws {

        let container = try decoder.container(keyedBy: CodingKeys.self)
        let response = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .response)

        view = try response.decode([ViewCodable].self, forKey: .view)
    }
}

extension CompleteResponse {

    func getLocation() -> Location? {

        guard let codableView = self.view.first else {
            return .none
        }

        guard let result = codableView.Result.first else {
            return .none
        }

        return result.Location
    }
}


// MARK: - View
struct ViewCodable: Decodable {

    let Result: [ResultCodable]
}

// MARK: - Result
struct ResultCodable: Decodable {

    let Location: Location
}

// MARK: - Location
struct Location: Decodable {

    let locationId: String
    let locationType: String

    let displayPosition: DisplayPosition
    let address: Address

    enum CodingKeys: String, CodingKey {
        case locationId = "LocationId"
        case locationType = "LocationType"
        case displayPosition = "DisplayPosition"
        case address = "Address"
    }

    init(from decoder: Decoder) throws {

        let container = try decoder.container(keyedBy: CodingKeys.self)

        locationId = try container.decode(String.self, forKey: .locationId)
        locationType = try container.decode(String.self, forKey: .locationType)

        displayPosition = try container.decode(DisplayPosition.self, forKey: .displayPosition)
        address = try container.decode(Address.self, forKey: .address)
    }
}

// MARK: - Address
struct Address: Decodable {
    let label, country: String?
    let postalCode, city, street: String?

    enum CodingKeys: String, CodingKey {
        case label = "Label"
        case country = "Country"
        case city = "City"
        case street = "Street"
        case postalCode = "PostalCode"
    }
}

// MARK: - DisplayPosition
struct DisplayPosition: Codable {
    let latitude, longitude: Double

    enum CodingKeys: String, CodingKey {
        case latitude = "Latitude"
        case longitude = "Longitude"
    }
}

// NOTE:
// Should improve for a better solution asap,
// Get location data directly and avoid unecessary wrapper models
/*
struct Location: Decodable{
    
    let locationId: String
    let locationType: String
    let latitude: Double
    let longitude: Double
    
    enum CodingKeys: String, CodingKey {
        case response = "Response"
        case view = "View"
        case result = "Result"
        case location = "Location"
        case locationId = "LocationId"
        case locationtype = "LocationType"
        case displayPosition = "DisplayPosition"
    }
    
    enum PositionKeys: String, CodingKey {
        case latitude = "Latitude"
        case longitude = "Longitude"
    }
    
    init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let response = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .response)
        
        var viewList = try response.nestedUnkeyedContainer(forKey: .view)
        
        let resultContainer = try viewList.nestedContainer(keyedBy: CodingKeys.self)
        var resultList = try resultContainer.nestedUnkeyedContainer(forKey: .result)
        
        let locationContainer = try resultList.nestedContainer(keyedBy: CodingKeys.self)
        
        locationId = try locationContainer.decode(String.self, forKey: .locationId)
        locationType = try locationContainer.decode(String.self, forKey: .locationtype)
        
        let displayPosition = try locationContainer.nestedContainer(keyedBy: PositionKeys.self, forKey: .displayPosition)
        latitude = try displayPosition.decode(Double.self, forKey: .latitude)
        longitude = try displayPosition.decode(Double.self, forKey: .longitude)
    }
}
*/

//
//  Suggestion.swift
//  Locations
//
//  Created by João Pedro Silva on 19/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.
//

struct Suggestion: Decodable {
    
    let label: String
    let language: String
    let countryCode: String
    let locationId: String
    let matchLevel: String
    let distance: Int = 0
}

extension Suggestion: ListViewItemImplementable{
    
    func title() -> String {
        return label
    }
    
    func subTitle() -> String {
        
        return "\(language) \(countryCode)"
    }
}

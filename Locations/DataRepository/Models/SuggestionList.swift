//
//  SuggestionList.swift
//  Locations
//
//  Created by João Pedro Silva on 19/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.
//

import Foundation

struct SuggestionList: Decodable {
    
    let suggestions: [Suggestion]
}

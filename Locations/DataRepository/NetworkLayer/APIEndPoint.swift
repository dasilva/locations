//
//  APIEndPoint.swift
//  Locations
//
//  Created by João Pedro Silva on 15/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.
//

import UIKit

protocol APIEndPointProtocol{
    
    var baseURL: URL { get }
    var httpMethod: HTTPMethod { get }
    var path: String { get }
    var parameters: RequestParameters { get }
}

public typealias Parameters = [String: Any]
public enum RequestParameters {
    case none
    case request(Parameters)
}

public enum APIEndPoint{
    
    case locations(searchText: String, coordinates: TupleCoordinate?)
    case location(locationid: String)
}

extension APIEndPoint: APIEndPointProtocol{

    var baseURL: URL {
        switch self {
        case .locations:
            guard let url = URL(string: "https://autocomplete.geocoder.api.here.com/") else { fatalError("baseURL could not be configured.")}
            return url
        default:
            guard let url = URL(string: "https://geocoder.api.here.com/") else { fatalError("baseURL could not be configured.")}
            return url
        }
    }
    
    var path: String {
        switch self {
        case .locations:
            return "6.2/suggest.json"
        case .location:
            return "6.2/geocode.json"
        }
    }
    
    var httpMethod: HTTPMethod {
        switch self {
        case .locations, .location:
            return .get
        }
    }
    
    var parameters: RequestParameters {
        switch self {
        case .locations(let searchText, let coordinate):
            
            var requestParamentes: [String: Any] = ["app_id": ApiConstants.apiID,
                                     "app_code": ApiConstants.apiCode,
                                     "query": searchText]
            
            if let coordinate = coordinate {
                requestParamentes.updateValue("\(coordinate.0), \(coordinate.1)", forKey: "prox")
            }
            
            return .request(requestParamentes)
            
        case .location(let locationid):
            return .request(["app_id": ApiConstants.apiID,
                          "app_code": ApiConstants.apiCode,
                          "locationid": locationid])
        }
    }
}

//
//  APIRouter.swift
//  Locations
//
//  Created by João Pedro Silva on 15/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.
//

import Foundation

public typealias APIRouterCompletion = (_ data: Data?,_ response: URLResponse?,_ error: Error?)->()

protocol NetworkRouter: class {

    associatedtype EndPoint: APIEndPointProtocol
    func request(_ route: EndPoint, completion: @escaping APIRouterCompletion)
    func cancel()
}

class APIRouter<EndPoint: APIEndPointProtocol>: NetworkRouter{
    
    private var task: URLSessionTask?
    
    func request(_ route: EndPoint, completion: @escaping APIRouterCompletion) {
        
        let session = URLSession.shared
        do {
            let request = try self.request(from: route)
            request.debugLog()
            task = session.dataTask(with: request, completionHandler: { data, response, error in
                completion(data, response, error)
                
                print(data as Any)
                //print(response)
                print(error.debugDescription)
            })
        }catch {
            completion(nil, nil, error)
        }
        self.task?.resume()
    }

    func cancel() {
        self.task?.cancel()
    }
    
    fileprivate func request(from route: APIEndPointProtocol) throws -> URLRequest {
        
        var request = URLRequest(url: route.baseURL.appendingPathComponent(route.path))
        request.httpMethod = route.httpMethod.rawValue
        
        switch route.parameters {
        case .none:
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        case .request(let parameters):
  
            guard let url = request.url else{
                //Should create error
                return request
            }
            
            if var urlComponents = URLComponents(url: url,
                                                 resolvingAgainstBaseURL: false){
                
                if let items = APIRouter.create(parameters) {
                    urlComponents.queryItems = items
                }
                
                request.url = urlComponents.url
            }
        }
        
        return request
    }

    static func create(_ parameters: [String: Any]?) -> [URLQueryItem]? {
        
        var queryItems = [URLQueryItem]()
        
        guard let unwrappedParameters = parameters else {
            return nil
        }
        
        for (key, value) in unwrappedParameters {
            let strValue = String(describing: value)
            queryItems.append(URLQueryItem(name: key, value: strValue))
        }
        
        return queryItems
    }
}

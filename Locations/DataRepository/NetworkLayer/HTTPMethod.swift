//
//  HTTPMethod.swift
//  Locations
//
//  Created by João Pedro Silva on 15/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.
//

import Foundation

public enum HTTPMethod : String {
    case get     = "GET"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
}

//
//  NetworkLogger.swift
//  Locations
//
//  Created by João Pedro Silva on 15/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.
//

import Foundation

extension URLRequest {
    
    public func debugLog() {
        print(self)
    }
}

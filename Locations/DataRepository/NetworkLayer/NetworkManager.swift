//
//  NetworkManager.swift
//  Locations
//
//  Created by João Pedro Silva on 15/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.
//

import Foundation

enum NetworkErrorResponse: String {

    case authenticationError = "Should be connected"
    case requestError = "Request error"
    case outdated = "The url you requested is outdated."
    case failed = "Network failed."
    case noData = "Response returned with no data to decode."
    case unableToDecode = "We could not decode the response."
}

enum Result<T> {

    case success(T)
    case failure(NetworkErrorResponse)
}

public typealias TupleCoordinate = (String, String)

protocol NetworkManagerProtocol {

    func getLocations(for searchText: String,
                      coordinate: TupleCoordinate?,
                      completion: @escaping(Result<SuggestionList>) -> Void)

    func getLocation(for locationid: String, completion: @escaping(Result<Location>) -> Void)
}

struct NetworkManager: NetworkManagerProtocol {

    //MARK: DI Vars
    let router = APIRouter<APIEndPoint>()

    //MARK: - Protocol Methods
    func getLocations(for searchText: String,
                      coordinate: TupleCoordinate? = nil,
                      completion: @escaping (Result<SuggestionList>) -> Void) {

        router.request(.locations(searchText: searchText, coordinates: coordinate)) { (data, response, error) in

            if error != nil {
                completion(Result.failure(.requestError))
            }

            if let response = response as? HTTPURLResponse {
                let result = self.handleNetworkResponse(response)
                switch result {
                case .success:
                    guard let responseData = data else {
                        completion(Result.failure(.noData))
                        return
                    }
                    do {
                        let apiResponse = try JSONDecoder().decode(SuggestionList.self, from: responseData)
                        completion(Result.success(apiResponse))
                    } catch {
                        print(error)
                        completion(Result.failure(.unableToDecode))
                    }
                case .failure(let networkFailureError):
                    completion(Result.failure(networkFailureError))
                }
            }
        }
    }

    func getLocation(for locationid: String, completion: @escaping (Result<Location>) -> Void) {

        router.request(.location(locationid: locationid)) { (data, response, error) in

            if error != nil {
                completion(Result.failure(.requestError))
            }

            if let response = response as? HTTPURLResponse {
                let result = self.handleNetworkResponse(response)
                switch result {
                case .success:
                    guard let responseData = data else {
                        completion(Result.failure(.noData))
                        return
                    }
                    do {
                        
                        let apiResponse = try JSONDecoder().decode(CompleteResponse.self, from: responseData)
                        if let location = apiResponse.getLocation() {
                            
                            completion(Result.success(location))
                        } else {
                            
                            completion(Result.failure(.unableToDecode))
                        }
                    } catch {
                            print(error)
                            completion(Result.failure(.unableToDecode))
                        }
                case .failure(let networkFailureError):
                            completion(Result.failure(networkFailureError))
                    }
                }
            }
        }
    
    fileprivate func handleNetworkResponse(_ response: HTTPURLResponse) -> Result<String> {
        
        switch response.statusCode {
        case 200...299: return .success("sucess")
        case 401...500: return .failure(NetworkErrorResponse.authenticationError)
        case 501...599: return .failure(NetworkErrorResponse.requestError)
        case 600: return .failure(NetworkErrorResponse.outdated)
        default: return .failure(NetworkErrorResponse.failed)
        }
    }
}

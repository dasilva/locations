//
//  Repository.swift
//  Locations
//
//  Created by João Pedro Silva on 14/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.
//
import Foundation

protocol LocationsFavoritesDataProvider {

    func saveToFavourites(location: Location)
    func removeFromFavourites(location: Location)
}

protocol LocationDataProvider {
    
    func getLocation(for locationid: String, completion: @escaping(Result<Location>) -> Void)
}

protocol LocationsListDataProvider {

    func getLocations(for searchText: String, coordinate: Coordinate?, completion: @escaping(Result<SuggestionList>) -> Void)
}

class Repository {

    let networkManager: NetworkManagerProtocol!
    let persistanceManager: PersistanceManager!

    required init(with networkManager: NetworkManagerProtocol, and persistanceManager: PersistanceManager) {

        self.networkManager = networkManager
        self.persistanceManager = persistanceManager
    }
}

extension Repository: LocationsListDataProvider {
    
    func getLocations(for searchText: String,
                      coordinate: Coordinate?,
                      completion: @escaping (Result<SuggestionList>) -> Void) {
        
        var tuple: TupleCoordinate?
        if let coordinate = coordinate {
            tuple = (String(format: "%f", coordinate.latitude), String(format: "%f", coordinate.longitude))
        }
        
        networkManager.getLocations(for: searchText,
                                    coordinate: tuple,
                                    completion: completion)
    }
}

extension Repository: LocationDataProvider{
    
    func getLocation(for locationid: String, completion: @escaping (Result<Location>) -> Void) {
        
        networkManager.getLocation(for: locationid, completion: completion)
    }
}

extension Repository: LocationsFavoritesDataProvider {

    func saveToFavourites(location: Location) {
       persistanceManager.saveToFavourites(location: location)
    }

    func removeFromFavourites(location: Location) {
        persistanceManager.removeFromFavourites(location: location)
    }
}

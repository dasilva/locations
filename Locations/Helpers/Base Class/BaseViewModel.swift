//
//  BaseViewModel.swift
//
//
//  Created by João Pedro Silva on 14/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.
//

import UIKit

/// This class should implement the main connection used in most of case of V-VM model connection: Error and Loader funtionalities
/// If the class shouldn t have this funtionalities implement in th class and not override this
class BaseViewModel: NSObject, ViewModelProtocol {
    
    //MARK:
    var updateLoadingStatus: ((Bool)->())?
    var presentErrorMessage: ((String)->())?

    var isLoading: Bool? {
        didSet {
            if let isLoading = isLoading {
                self.updateLoadingStatus?(isLoading)
            }
        }
    }
    
    var errorMessage: String? {
        didSet {
            if let message = errorMessage{
                self.presentErrorMessage?(message)
            }
        }
    }
}

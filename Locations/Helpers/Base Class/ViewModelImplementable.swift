//
//  ViewModelImplementable.swift
//  Locations
//
//  Created by João Pedro Silva on 14/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.
//

import UIKit

protocol ViewModelImplementable: LoaderDisplaying {

}

extension ViewModelImplementable where Self: UIViewController{

    mutating func updateLoaderState(_ show: Bool){
        
        if show{
            displayLoader()
        } else{
            hideLoader()
        }
    }
}

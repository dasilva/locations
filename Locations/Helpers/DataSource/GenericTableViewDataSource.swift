//
//  GenericTableViewDataSource.swift
//  mobime
//
//  Created by João Pedro Silva on 17/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.
//

import UIKit

public typealias TableViewCellConfigureBlock = (Any, Any) -> Void

class GenericTableViewDataSource: NSObject, UITableViewDataSource {

    private var items: Array<Any> = []
    private var cellConfigureBlock: TableViewCellConfigureBlock?
    private var cellIdentifier: String?
    
    init(items: Array<Any> = [], cellIdentifier:String, configureBlock: @escaping TableViewCellConfigureBlock) {
        super.init()
        
        self.items = items
        self.cellConfigureBlock = configureBlock
        self.cellIdentifier = cellIdentifier
    }
    
    //MARK: DataSource Helper Methods
    func itemAtIndexPath(indexPath:IndexPath)->Any{
        return items[indexPath.row]
    }
    
    func numberItems() -> Int{
        return items.count
    }
    
    func updateItems(items: Array<Any>){
        self.items = items
    }

    //MARK: TableView DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier!, for: indexPath)
        let item = itemAtIndexPath(indexPath: indexPath)
        self.cellConfigureBlock?(cell, item)
        return cell
    }
}

//
//  DeviceLocation.swift
//  Locations
//
//  Created by João Pedro Silva on 19/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.
//

import CoreLocation

typealias Coordinate = CLLocationCoordinate2D

protocol DeviceLocation {
    var coordinate: Coordinate { get }
}

extension CLLocation: DeviceLocation { }

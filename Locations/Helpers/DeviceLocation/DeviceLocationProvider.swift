//
//  DeviceLocationProvider.swift
//  Locations
//
//  Created by João Pedro Silva on 19/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.
//

import Foundation

enum DeviceLocationError: Swift.Error {
    case canNotBeLocated
}

typealias DeviceLocationCompletionBlock = (DeviceLocation?, DeviceLocationError?) -> Void

protocol DeviceLocationProvider {
    func findUserLocation(then: @escaping DeviceLocationCompletionBlock)
}

//
//  DeviceLocationService.swift
//  Locations
//
//  Created by João Pedro Silva on 19/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.
//

import UIKit
import CoreLocation

class DeviceLocationService: NSObject, DeviceLocationProvider {
    
    fileprivate var provider: LocationProvider
    fileprivate var locationCompletionBlock: DeviceLocationCompletionBlock?
    
    init(with provider: LocationProvider) {
        self.provider = provider
        super.init()
        
        self.provider.delegate = self
    }
    
    func findUserLocation(then: @escaping DeviceLocationCompletionBlock) {
        self.locationCompletionBlock = then
        if provider.isUserAuthorized {
            provider.requestLocation()
        } else {
            provider.requestWhenInUseAuthorization()
        }
    }
}

extension DeviceLocationService: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            provider.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let location = locations.last {
            locationCompletionBlock?(location, nil)
        } else {
            locationCompletionBlock?(nil, .canNotBeLocated)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        locationCompletionBlock?(nil, .canNotBeLocated)

    }
}

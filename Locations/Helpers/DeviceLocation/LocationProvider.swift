//
//  LocationProvider.swift
//  Locations
//
//  Created by João Pedro Silva on 19/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.
//

import CoreLocation

protocol LocationProvider {
    
    var isUserAuthorized: Bool { get }
    var delegate: CLLocationManagerDelegate? { get set}

    func requestWhenInUseAuthorization()
    func requestLocation()
}

extension CLLocationManager: LocationProvider {
    
    var isUserAuthorized: Bool {
        return CLLocationManager.authorizationStatus() == .authorizedWhenInUse
    }
}

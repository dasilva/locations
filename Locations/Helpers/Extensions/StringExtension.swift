//
//  StringExtension.swift
//  Locations
//
//  Created by João Pedro Silva on 18/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.
//

import Foundation

extension String{
    
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
}

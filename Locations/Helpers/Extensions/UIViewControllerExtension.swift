//
//  UIViewControllerExtension.swift
//  Locations
//
//  Created by João Pedro Silva on 18/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func presentMessage(withTitle title: String = "", message: String = "", completion: (() -> Void)? = nil) {
            
            let alertController = UIAlertController(title: title.localized, message: message.localized, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "ok_message".localized, style: .default){_ in})
            self.present(alertController, animated: true, completion: completion)
    }
}

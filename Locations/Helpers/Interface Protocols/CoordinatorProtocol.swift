//
//  CoordinatorProtocol.swift
//  Locations
//
//  Created by João Pedro Silva on 14/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.

import Foundation
import UIKit

/// Based Protocol to be used by all Coordinators
/// Should be used as reference between Coordinators
protocol Coordinator: class{
    
    /// Array with all the sub coordinators initialized in a coordinator
    //  var childCoordinators: [Coordinator] { get set }
    
    /// Entry point starting the coordinator
    ///
    /// - Returns: ViewController created on the Coordinator
    func load()-> UIViewController
}


protocol CoordinatorDelegate {
    
    func didFinish(from coordinator: Coordinator)
}

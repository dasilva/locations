//
//  ListViewImplementable.swift
//  Locations
//
//  Created by João Pedro Silva on 17/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.
//

import Foundation

protocol ListViewItemImplementable {
    
    func title()-> String
    func subTitle()-> String
}

extension ListViewItemImplementable{
    
    //Make Subtitle optional
    func subTitle()-> String{
        return ""
    }
}

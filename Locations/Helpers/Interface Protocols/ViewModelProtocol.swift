//
//  ViewModelProtocol.swift
//  mobime
//
//  Created by João Pedro Silva on 14/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.
//

import UIKit

///Based Protocol for ViewModel's Base
protocol ViewModelProtocol {
    
    var updateLoadingStatus: ((Bool)->())? {get set}
    var presentErrorMessage: ((String)->())? {get set}
}

//
//  LoaderDisplaying.swift
//  Locations
//
//  Created by João Pedro Silva on 14/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.
//

import UIKit

protocol LoaderDisplaying {

    var loaderView: UIView? {get set}

    mutating func displayLoader()
    mutating func hideLoader()
}

extension LoaderDisplaying where Self: UIViewController{
    
    mutating func displayLoader(){
    
        if loaderView == nil{
            
            let loader = UIView.init(frame: self.view.bounds)
            loader.backgroundColor = UIColor.init(red: 0.5,
                                                  green: 0.5,
                                                  blue: 0.5,
                                                  alpha: 0.5)
            let activityIndicator = UIActivityIndicatorView.init(style: .whiteLarge)
            activityIndicator.startAnimating()
            activityIndicator.center = loader.center
            
            
            //should be release
            loader.addSubview(activityIndicator)
            self.view.addSubview(loader)
            
            loaderView = loader
        }
    }
    
    mutating func hideLoader(){
        
        loaderView?.removeFromSuperview()
        loaderView = nil
    }
}

//
//  NibCreatable.swift
//  mobime
//
//  Created by João Pedro Silva on 06/02/2019.
//  Copyright © 2019 CEIIA. All rights reserved.
//

import UIKit

protocol NibCreatable {
    static var nibName: String {get}
}

extension NibCreatable where Self: UIViewController{
    
    static var nibName: String{
        return "\(self)"
    }
}

extension NibCreatable where Self: UITableViewCell{
    
    static var nibName: String{
        return "\(self)"
    }
}

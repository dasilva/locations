//
//  LocationDetailRepository.swift
//  Locations
//
//  Created by João Pedro Silva on 18/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.
//

import UIKit

typealias LocationDetailDataProviderProtocol = LocationDataProvider & LocationsFavoritesDataProvider

protocol LocationDetailRepositoryProtocol {
    
    var dataProvider: LocationDetailDataProviderProtocol {get}
    init(with dataProvider: LocationDetailDataProviderProtocol)
    
    func getLocationsDetail(for locationId: String, completion: @escaping(Result<Location>)->Void)
}

class LocationsDetailRepository: LocationDetailRepositoryProtocol {
    
    var dataProvider: LocationDetailDataProviderProtocol
    
    required init(with dataProvider: LocationDetailDataProviderProtocol) {
        
        self.dataProvider = dataProvider
    }
    
    func getLocationsDetail(for locationId: String, completion: @escaping (Result<Location>) -> Void) {
        
        dataProvider.getLocation(for: locationId, completion: completion)
    }
    
    func saveToFavourites(location: Location){
        
        dataProvider.saveToFavourites(location: location)
    }
    
    func removeFromFavourites(location: Location){
        
        dataProvider.removeFromFavourites(location: location)
    }
}

//
//  LocationDetailCoordinator.swift
//  Locations
//
//  Created by João Pedro Silva on 17/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.
//

import UIKit

class LocationDetailCoordinator: Coordinator{
    
    //MARK: - DI variables
    var delegate: CoordinatorDelegate?
    var dataProvider: LocationDetailDataProviderProtocol
    var locationid: String

    fileprivate var viewController: UIViewController {
        
        let locationListRepository = LocationsDetailRepository(with: dataProvider)
        let viewModel = LocationDetailViewModel(with: locationListRepository, and: locationid)
        let viewController = LocationDetailViewController(nibName: LocationDetailViewController.nibName, bundle: nil)
        viewController.viewModel = viewModel
        
        return viewController
    }
    
    //MARK: - Initializers
    required init(with dataProvider: LocationDetailDataProviderProtocol, locationid: String) {
        self.dataProvider = dataProvider
        self.locationid = locationid
    }
    
    func load() -> UIViewController {
        
        return viewController
    }
}

//
//  LocationDetailViewController.swift
//  Locations
//
//  Created by João Pedro Silva on 18/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.
//
import UIKit
import MapKit

class LocationDetailViewController: UIViewController, ViewModelImplementable, NibCreatable {
    
    //MARK:- DI Vars
    var viewModel: LocationDetailViewModelProtocol!{
        didSet{
            viewModel.updateLoadingStatus = { [weak self] (loading) in
                DispatchQueue.main.async {
                    self?.updateLoaderState(loading)
                }
            }
            
            viewModel.presentErrorMessage = { [weak self] (messageString) in
                DispatchQueue.main.async {
                    self?.presentMessage(message: messageString)
                }
            }
        }
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var mapKit: MKMapView!
    @IBOutlet weak var streetLabel: UILabel!
    @IBOutlet weak var postalCodeLabel: UILabel!
    @IBOutlet weak var coordinatesLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    var loaderView: UIView?

    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapKit.delegate = self
        
        viewModelBind()
        viewModel.getDetail()
    }
    
    //MARK: - Private Methods
    fileprivate func viewModelBind(){
        
        viewModel.mapAnnotation = { [weak self] mapAnnotation in
            
            self?.mapKit.addAnnotation(mapAnnotation)
        }
        
        viewModel.location = { [weak self] location in
            
            guard let self = self else {
                return
            }

            self.streetLabel.text = location.address.street ?? ""
            self.postalCodeLabel.text = location.address.postalCode ?? ""
            self.coordinatesLabel.text = "\(location.displayPosition.latitude), \(location.displayPosition.longitude)"
            
            //TBD
            //for distance get the location provider and calculate the distance to the displayPosition
        }
    }
}

extension LocationDetailViewController: MKMapViewDelegate{
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if(annotation.isKind(of: MKUserLocation.self)) {
            (annotation as? MKUserLocation)?.title = ""
            return .none
        }
        
        guard let locationAnnotation = annotation as? LocationMapAnnotation else{
            return .none
        }
        
        var annotationView: MKAnnotationView? = mapView.dequeueReusableAnnotationView(withIdentifier: locationAnnotation.identifier)
        
        if (annotationView == nil){
            
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: locationAnnotation.identifier)
        }
        
        annotationView?.annotation = annotation;
        return annotationView
    }
}

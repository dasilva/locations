//
//  LocationDetailViewModel.swift
//  Locations
//
//  Created by João Pedro Silva on 18/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.
//

import Foundation

protocol LocationDetailViewModelProtocol: ViewModelProtocol {
    
    var idLocation: String { get}
    var repository: LocationDetailRepositoryProtocol {get}

    // Bind Varible
    // Normaly if the project had RX this will be an Observable
    var location:((Location) -> Void)? {get set}
    var mapAnnotation:((LocationMapAnnotation) -> Void)? {get set}

    init(with repository: LocationDetailRepositoryProtocol, and idLocation: String)

    func getDetail()
}

class LocationDetailViewModel: BaseViewModel, LocationDetailViewModelProtocol{
    
    //MARK:- DI Vars
    var repository: LocationDetailRepositoryProtocol
    let idLocation: String
    
    //MARK:- Bind Vars
    var location: ((Location) -> Void)?
    var mapAnnotation: ((LocationMapAnnotation) -> Void)?

    required init(with repository: LocationDetailRepositoryProtocol, and idLocation: String) {
        
        self.repository = repository
        self.idLocation = idLocation
    }
    
    func getDetail() {
        
        isLoading = true
        repository.getLocationsDetail(for: idLocation) {  [weak self] (result) in
            
            self?.isLoading = false
            switch result{
            case .success(let location):
                
                guard let self = self else{
                    return
                }
                
                self.mapAnnotation?(LocationMapAnnotation(with: location))
                self.location?(location)
                
            case .failure(let error):  self?.errorMessage = error.rawValue
            }
        }
    }
}

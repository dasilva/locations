//
//  LocationsListCoordinator.swift
//  Locations
//
//  Created by João Pedro Silva on 14/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.
//
import UIKit
import CoreLocation

class LocationsListCoordinator {

    var delegate: CoordinatorDelegate?
    var repository: Repository

    fileprivate var navigationController: UINavigationController {

        return UINavigationController(rootViewController: self.viewController)
    }

    fileprivate var viewController: UIViewController {

        let locationProvider = DeviceLocationService(with: CLLocationManager())
        let locationListRepository = LocationsListRepository(with: repository,
                                                             locationProvider: locationProvider)
        let viewModel = LocationsListViewModel(with: self, repository: locationListRepository)
        let viewController = LocationsListViewController(nibName: LocationsListViewController.nibName, bundle: nil)
        viewController.viewModel = viewModel

        return viewController
    }

    //MARK: - Initializers
    required init(with repository: Repository) {
        self.repository = repository
    }
}

extension LocationsListCoordinator: Coordinator {

    func load() -> UIViewController {

        return navigationController
    }
}

extension LocationsListCoordinator: LocationsListViewModelDelegate {

    func selectedLocation(with id: String) {

        let detailCoordinator = LocationDetailCoordinator(with: repository,
                                                          locationid: id)
        let viewController = detailCoordinator.load()
        self.navigationController.pushViewController(viewController, animated: true)
    }
}

//
//  LocationsListRepository.swift
//  Locations
//
//  Created by João Pedro Silva on 14/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.
//

import UIKit

typealias LocationListDataProviderProtocol = LocationsListDataProvider & LocationsFavoritesDataProvider

protocol LocationsListRepositoryProtocol {
    
    var dataProvider: LocationsListDataProvider {get}
    var locationProvider: DeviceLocationProvider {get}
    var deviceLocation: DeviceLocation? {get}

    init(with dataProvider: LocationListDataProviderProtocol, locationProvider: DeviceLocationProvider)
    
    func getLocations(for searchText: String, completion: @escaping(Result<SuggestionList>)->Void)
}

class LocationsListRepository: LocationsListRepositoryProtocol {
    
    //MARK: DI Vars
    var dataProvider: LocationsListDataProvider
    var locationProvider: DeviceLocationProvider
    
    //MARK: Vars
    var deviceLocation: DeviceLocation?

    //MARK: Initializers
    required init(with dataProvider: LocationListDataProviderProtocol, locationProvider: DeviceLocationProvider) {
        
        self.dataProvider = dataProvider
        self.locationProvider = locationProvider
        
        self.requestUserLocation()
    }
    
    //MARK: Methods
    
    //Initial without the API Knlodge the approach was to get all the details items
    func getLocations(for searchText: String, completion: @escaping(Result<SuggestionList>)->Void){

        let searchTextTrimmed: String = searchText.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).lowercased()
        
        dataProvider.getLocations(for: searchTextTrimmed, coordinate: deviceLocation?.coordinate, completion: completion)
    }
    
    func requestUserLocation() {
        
        locationProvider.findUserLocation { [weak self] location, error in
            if error == nil {
                self?.deviceLocation = location
            } else {
                print("No Location")
            }
        }
    }
}




/*
 func getLocations(for searchText: String, completion: @escaping(Result<[SuggetionLocation]>)->Void){
 
 let searchTextTrimmed: String = searchText.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).lowercased()
 dataProvider.getLocations(for: searchTextTrimmed) { [weak self] (result) in
 
 //We need to get the location for each suggestion in order to get order the list<
 switch result{
 case .success(let suggestionList):
 
 var sugestionWithLocations = [SuggetionLocation]()
 
 let requestGroup = DispatchGroup()
 for suggestion in suggestionList.suggestions {
 
 requestGroup.enter()
 
 self?.dataProvider.getLocation(for: suggestion.locationId, completion: { (result) in
 
 switch result{
 case .success(let location):
 
 sugestionWithLocations.append(SuggetionLocation(location: location, suggestion: suggestion))
 case .failure: break
 }
 
 requestGroup.leave()
 })
 }
 
 requestGroup.notify(queue: .main) {
 completion(Result.success(sugestionWithLocations))
 }
 
 case .failure(let error):
 //Pass error to ViewModel
 completion(Result.failure(error))
 }
 }
 }*/

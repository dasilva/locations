//
//  LocationsListViewController.swift
//  Locations
//
//  Created by João Pedro Silva on 14/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.
//
import UIKit

class LocationsListViewController: UIViewController, ViewModelImplementable, NibCreatable {
    
    //MARK:- DI Vars
    var viewModel: LocationsListViewModelProtocol!{
        didSet{
            viewModel.updateLoadingStatus = { [weak self] (loading) in
                DispatchQueue.main.async {
                    self?.updateLoaderState(loading)
                }
            }
            
            viewModel.presentErrorMessage = { [weak self] (messageString) in
                DispatchQueue.main.async {
                    self?.presentMessage(message: messageString)
                }
            }
        }
    }

    //MARK:- IBOutlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    var loaderView: UIView?

    //MARK:- Vars
    var dataSource: GenericTableViewDataSource?
    var lastSearchedText: String?

    //MARK:- Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = Strings.Suggestions.title
        
        tableView.delegate = self
        tableView.register(UINib(nibName: ListViewCell.nibName, bundle: nil),
                           forCellReuseIdentifier: ListViewCell.reuseIdentifier())

        dataSource = GenericTableViewDataSource(cellIdentifier: ListViewCell.reuseIdentifier(),
                                                          configureBlock: { (cell, model) in
                                                            
                                                            guard let cell = cell as? ListViewCell, let model = model as? ListViewItemImplementable else{
                                                                return
                                                            }
                                                            
                                                            cell.config(for: model)
        })
        tableView.dataSource = dataSource
        
        searchBar.delegate = self
        viewModelBind()
    }
    
    //MARK: - Private Methods
    fileprivate func viewModelBind(){
        
        viewModel.locations = { [weak self] locations in
            
            self?.updateTableView(with: locations)
        }
    }
    
    fileprivate func updateTableView(with locations: [Suggestion]){
        
        dataSource?.updateItems(items: locations)
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
        }
    }
}

//MARK: - TableView Delegate
extension LocationsListViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        searchBar.resignFirstResponder()
        
        guard let model = dataSource?.itemAtIndexPath(indexPath: indexPath),
            let suggestion = model as? Suggestion else {
            return
        }
        
        viewModel.didSelect(suggestion)
    }
}

//MARK: - SearchBar Updates
extension LocationsListViewController: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        searchForText("")
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        searchForText(searchText)
    }
    
    func searchForText(_ searchText: String){
        
        // to limit multiple requests activity, reload half a second after last key press.
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.searchLocations), object: lastSearchedText)
        //need to save reference the last searched text so it can be canceled if needed
        lastSearchedText = searchText
        self.perform(#selector(self.searchLocations), with: lastSearchedText, afterDelay: 0.5)
    }
    
    @objc func searchLocations( searchText: String?){
    
        guard let text = searchText, !text.isEmpty else{
            
            updateTableView(with: [])
            return
        }
        
        viewModel.search(for: text)
    }
}

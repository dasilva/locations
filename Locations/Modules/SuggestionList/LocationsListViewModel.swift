//
//  LocationsListViewModel.swift
//  Locations
//
//  Created by João Pedro Silva on 14/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.
//

import UIKit

protocol LocationsListViewModelDelegate: class {
    func selectedLocation(with id: String)
}

protocol LocationsListViewModelProtocol: ViewModelProtocol {
    
    init(with delegate: LocationsListViewModelDelegate?,
         repository: LocationsListRepository)
    
    // Bind Varible
    // Normaly if the project had RX this will be an Observable
    var locations:(([Suggestion]) -> Void)? {get set}
    
    // Public Methods for the View
    func search(for text: String)
    func didSelect(_ suggestion: Suggestion)
}

class LocationsListViewModel: BaseViewModel, LocationsListViewModelProtocol{

    //MARK:- DI Vars
    weak var delegate: LocationsListViewModelDelegate?
    let repository: LocationsListRepositoryProtocol!

    //MARK: Bind Vars
    var locations: (([Suggestion]) -> Void)?

    //MARK: Vars
    required init(with delegate: LocationsListViewModelDelegate? = nil, repository: LocationsListRepository) {
        
        self.delegate = delegate
        self.repository = repository
    }
    
    //MARK: Public Methods
    func search(for text: String) {
        
        isLoading = true
        repository.getLocations(for: text) { [weak self] (result) in
            
            self?.isLoading = false

            switch result{
            case .success(let locationList): self?.orderLocationsByDistance(locationList.suggestions)
            case .failure(let error):  self?.errorMessage = error.rawValue
            }
        }
    }
    
    func orderLocationsByDistance(_ suggestions: [Suggestion]){
        
        if let locations = locations{
            
            locations(suggestions.sorted(by: { $0.distance < $1.distance }))
        }
    }
    
    func didSelect(_ suggestion: Suggestion){

        self.delegate?.selectedLocation(with: suggestion.locationId)
    }
}

//
//  ListViewCell.swift
//  Locations
//
//  Created by João Pedro Silva on 17/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.
//

import UIKit

class ListViewCell: UITableViewCell, NibCreatable {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
}

extension ListViewCell{
    
    class func reuseIdentifier() -> String {
        return "ListViewCell"
    }
    
    func config(for model: ListViewItemImplementable){
        
        titleLabel.text = model.title()
        subTitleLabel.text = model.subTitle()
    }
}

//
//  LocationMapAnnotation.swift
//  Locations
//
//  Created by João Pedro Silva on 19/05/2019.
//  Copyright © 2019 com.dasilva. All rights reserved.
//

import Foundation
import MapKit

class LocationMapAnnotation: NSObject, MKAnnotation {

    var identifier = ""
    var coordinate: CLLocationCoordinate2D

    init(with location: Location, reuseIdentifier: String = "LocationMapAnnotation") {

        coordinate = CLLocationCoordinate2D(latitude: location.displayPosition.latitude,
                                            longitude: location.displayPosition.longitude)
        self.identifier = reuseIdentifier
    }
}
